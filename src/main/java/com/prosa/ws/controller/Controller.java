package com.prosa.ws.controller;

import java.rmi.RemoteException;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prosa.ws.model.FindAllResponse;
import com.prosa.ws.model.Response;
import com.prosa.ws.model.SwaAddRequest;
import com.prosa.ws.model.SwaSearchRequest;
import com.prosa.ws.service.SwaService;

import io.swagger.annotations.Api;

@Api
@RestController
@RequestMapping("/api/v1/swa/")
public class Controller {

	private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);

	@Autowired
	private SwaService service;

	@PostMapping("add")
	public ResponseEntity<Response> add(@RequestBody SwaAddRequest request) throws RemoteException {
		LOGGER.info("{}", "Invoke operation add from client");
		Response response = service.invokeAddRequest(request);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping("find")
	public ResponseEntity<Response> find(@RequestBody SwaSearchRequest request) throws RemoteException {
		LOGGER.info("Invoke operation search {} from client", ToStringBuilder.reflectionToString(request));
		Response response = service.invokeSearch(request);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping("findAll")
	public FindAllResponse findAll() throws RemoteException {
		LOGGER.info("{}", "Invoke findAll from client");
		return service.findAll();
	}

}
