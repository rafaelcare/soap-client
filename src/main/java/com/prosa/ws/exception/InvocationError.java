package com.prosa.ws.exception;

public class InvocationError extends RuntimeException {

	private static final long serialVersionUID = 3865717897251318671L;

	public InvocationError(Exception e) {
		super(e);
	}

}
