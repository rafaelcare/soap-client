package com.prosa.ws.exception;

public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = 7070315745785952500L;

	public NotFoundException(String message) {
		super(message);
	}

}
