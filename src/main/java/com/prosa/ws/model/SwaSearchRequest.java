package com.prosa.ws.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SwaSearchRequest {

	private String key;
	private String keyData;
	private String fechaHora;
	private String user;

}
