package com.prosa.ws.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class MapEntry {

	private String key;
	private String value;

}
