package com.prosa.ws.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SwaAddRequest {

	private String key;
	private String keyData;

}
