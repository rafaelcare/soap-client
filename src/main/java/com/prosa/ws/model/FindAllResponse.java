package com.prosa.ws.model;

import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class FindAllResponse {

	private List<MapEntry> mapEntry;

}
