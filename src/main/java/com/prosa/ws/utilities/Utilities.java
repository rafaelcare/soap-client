package com.prosa.ws.utilities;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import com.prosa.ws.exception.InvocationError;

@Component
public class Utilities {

	public <T> T copyField(Object origin, Class<T> clazzDestination) {
		try {
			T destination = clazzDestination.newInstance();
			BeanUtils.copyProperties(destination, origin);
			return clazzDestination.cast(destination);
		} catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
			throw new InvocationError(e);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> List<T> copyFieldList(Object origin, Class<T> clazzDestination) {
		List<T> list = new ArrayList<>();
		if (origin instanceof Collection) {
			((Collection) origin).forEach(r -> list.add(copyField(r, clazzDestination)));
		}
		return list;
	}

	public String convertObjectToString(Object obj) {
		return ToStringBuilder.reflectionToString(obj, ToStringStyle.JSON_STYLE);
	}
}
