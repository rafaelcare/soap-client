package com.prosa.ws.service;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prosa.ws.client.OpenSwitchPortServiceStub;
import com.prosa.ws.client.OpenSwitchPortServiceStub.AddRequest;
import com.prosa.ws.client.OpenSwitchPortServiceStub.AddResponse;
import com.prosa.ws.client.OpenSwitchPortServiceStub.FindAllRequest;
import com.prosa.ws.client.OpenSwitchPortServiceStub.MapEntry;
import com.prosa.ws.client.OpenSwitchPortServiceStub.SwaRequest;
import com.prosa.ws.client.OpenSwitchPortServiceStub.SwaResponse;
import com.prosa.ws.exception.NotFoundException;
import com.prosa.ws.model.FindAllResponse;
import com.prosa.ws.model.Response;
import com.prosa.ws.model.SwaAddRequest;
import com.prosa.ws.model.SwaSearchRequest;
import com.prosa.ws.utilities.Utilities;

@Component
public class SwaService {

	@Autowired
	private OpenSwitchPortServiceStub stub;

	@Autowired
	private Utilities utilities;

	public Response invokeAddRequest(SwaAddRequest request) throws RemoteException {
		AddResponse response = stub.add(utilities.copyField(request, AddRequest.class));
		return utilities.copyField(response, Response.class);
	}

	public Response invokeSearch(SwaSearchRequest request) throws RemoteException {
		SwaResponse response = stub.swa(utilities.copyField(request, SwaRequest.class));

		if (!Optional.ofNullable(response).isPresent()
				|| response.getResponse().equalsIgnoreCase("Data not found '>_<'")) {
			throw new NotFoundException(
					String.format("Data not found to %s", utilities.convertObjectToString(request)));
		}

		return utilities.copyField(response, Response.class);
	}

	public FindAllResponse findAll() throws RemoteException {
		Optional<com.prosa.ws.client.OpenSwitchPortServiceStub.FindAllResponse> response = Optional
				.ofNullable(stub.findAll(new FindAllRequest()));

		FindAllResponse result = null;
		if (response.isPresent()) {
			result = new FindAllResponse();
			List<MapEntry> list = Arrays.asList(response.get().getMapEntry());
			result.setMapEntry(utilities.copyFieldList(list, com.prosa.ws.model.MapEntry.class));
		}
		return result;
	}

}
