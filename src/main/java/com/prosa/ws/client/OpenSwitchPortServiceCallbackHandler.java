
/**
 * OpenSwitchPortServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4  Built on : Apr 26, 2008 (06:24:30 EDT)
 */

package com.prosa.ws.client;

/**
 * OpenSwitchPortServiceCallbackHandler Callback class, Users can extend this
 * class and implement their own receiveResult and receiveError methods.
 */
public abstract class OpenSwitchPortServiceCallbackHandler {

	protected Object clientData;

	/**
	 * User can pass in any object that needs to be accessed once the NonBlocking
	 * Web service call is finished and appropriate method of this CallBack is
	 * called.
	 * 
	 * @param clientData
	 *            Object mechanism by which the user can pass in user data that will
	 *            be avilable at the time this callback is called.
	 */
	public OpenSwitchPortServiceCallbackHandler(Object clientData) {
		this.clientData = clientData;
	}

	/**
	 * Please use this constructor if you don't want to set any clientData
	 */
	public OpenSwitchPortServiceCallbackHandler() {
		this.clientData = null;
	}

	/**
	 * Get the client data
	 */

	public Object getClientData() {
		return clientData;
	}

	/**
	 * auto generated Axis2 call back method for findAll method override this method
	 * for handling normal response from findAll operation
	 */
	public void receiveResultfindAll(com.prosa.ws.client.OpenSwitchPortServiceStub.FindAllResponse result) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling error
	 * response from findAll operation
	 */
	public void receiveErrorfindAll(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 call back method for add method override this method for
	 * handling normal response from add operation
	 */
	public void receiveResultadd(com.prosa.ws.client.OpenSwitchPortServiceStub.AddResponse result) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling error
	 * response from add operation
	 */
	public void receiveErroradd(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 call back method for swa method override this method for
	 * handling normal response from swa operation
	 */
	public void receiveResultswa(com.prosa.ws.client.OpenSwitchPortServiceStub.SwaResponse result) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling error
	 * response from swa operation
	 */
	public void receiveErrorswa(java.lang.Exception e) {
	}

}
