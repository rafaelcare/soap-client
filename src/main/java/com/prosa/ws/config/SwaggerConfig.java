package com.prosa.ws.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;

import io.swagger.annotations.Api;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

	/**
	 * Create Swagger Api configuration
	 * 
	 * @author carpinteyror
	 * @return Swagger Docket
	 */
	@Bean
	public Docket sadrApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("com").apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.withClassAnnotation(Api.class)).paths(PathSelectors.any()).build()
				.pathMapping("/").genericModelSubstitutes(ResponseEntity.class).useDefaultResponseMessages(false);
	}

	/**
	 * Generate Api Info
	 * 
	 * @author carpinteyror
	 * @return Swagger API Info
	 */
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("soap-client").description("").version("").termsOfServiceUrl("").license("")
				.licenseUrl("").build();
	}

}
